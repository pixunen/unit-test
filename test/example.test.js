// test/example.test.js

const expect = require('chai').expect;
const should = require('chai').should();
const { assert } = require('chai');
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {
    before(() => {
        myvar = 1; // setup before testing
        console.log('Before testing...');
    });

    it('Myvar should exist', () => {
        should.exist(myvar);
    });

    it('Parametrized unit testing', () => {
        const result = mylib.sum(myvar, myvar); // 1 + 1
        expect(result).to.equal(myvar+myvar);
    });

    it('Should return 2 when using sum function with a=1, B=5', () => {
        const result = mylib.sum(1, 5);
        expect(result).to.equal(6);
    });

    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar'); // true
    });

    after(() => {
        console.log('After testing...');
    });
});